# rPimlac

This project is a further development of the previous *imlac* and *pymlac*
projects.  This one will use a Raspberry Pi as the emulation platform and
use the framebuffer directly.  Later developments may make the rPimlac system
run on boot, as is done on the
[pigfx video system for the RC2014 machines](https://github.com/fbergama/pigfx).

## sandbox

After fiddling with the compote resources, the best code base to build on is 
pageX/testXI.c.  It has smooth animation and saves/restores the cursor.


## Resources

https://raspberrycompote.blogspot.com/2012/12/low-level-graphics-on-raspberry-pi-part_9509.html

https://web.archive.org/web/20161104003307/http://www.directfb.net/

https://github.com/DirectFB/directfb

https://github.com/fbergama/pigfx
