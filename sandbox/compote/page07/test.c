/*
 * display.c
 *
 * A testbed for the real rPimlac display code.
 * Runs only in 16bpp mode!?
 *
 * Original work by J-P Rosti (a.k.a -rst- and 'Raspberry Compote')
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <linux/kd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <stdint.h>
#include "vcio.h"


// 'global' variables to store screen info
char *fbp = 0;
struct fb_var_screeninfo vinfo;
struct fb_fix_screeninfo finfo;


void put_pixel_RGB565(int x, int y, int r, int g, int b)
{
    // calculate the pixel's byte offset inside the buffer
    // note: x * 2 as every pixel is 2 consecutive bytes
    unsigned int pix_offset = x * 2 + y * finfo.line_length;

    // now this is about the same as 'fbp[pix_offset] = value'
    // but a bit more complicated for RGB565
    //unsigned short c = ((r / 8) << 11) + ((g / 4) << 5) + (b / 8);
    unsigned short c = ((r / 8) * 2048) + ((g / 4) * 32) + (b / 8);
    // write 'two bytes at once'
    *((unsigned short*)(fbp + pix_offset)) = c;

}

//---------------------------------------------------------------------
// Draw a line.
//---------------------------------------------------------------------

void draw_line(int x0, int y0, int x1, int y1, int r, int g, int b)
{
    int dx = x1 - x0;

    dx = (dx >= 0) ? dx : -dx; // abs()

    int dy = y1 - y0;

    dy = (dy >= 0) ? dy : -dy; // abs()

    int sx;
    int sy;

    if (x0 < x1)
      sx = 1;
    else
      sx = -1;

    if (y0 < y1)
      sy = 1;
    else
      sy = -1;

    int err = dx - dy;
    int e2;
    int done = 0;

    while (!done)
    {
        put_pixel_RGB565(x0, y0, r, g, b);

        if ((x0 == x1) && (y0 == y1))
	{
            done = 1;
	}
        else
       	{
            e2 = 2 * err;
            if (e2 > -dy)
	    {
                err = err - dy;
                x0 = x0 + sx;
            }
            if (e2 < dx)
	    {
                err = err + dx;
                y0 = y0 + sy;
            }
        }
    }
}

//---------------------------------------------------------------------
// Draw a rectangle.
//---------------------------------------------------------------------

void rectangle(int x, int y, int w, int h, int r, int g, int b)
{
    draw_line(x, y, x+w, y, r, g, b);
    draw_line(x, y, x, y+h, r, g, b);
    draw_line(x, y+h, x+w, y+h, r, g, b);
    draw_line(x+w, y, x+w, y+h, r, g, b);
}

//---------------------------------------------------------------------
// Draw a fuilled rectangle.
//---------------------------------------------------------------------

void fill_rect(int x, int y, int w, int h, int r, int g, int b)
{
    for (int cy = 0; cy < h; cy++)
    {
        for (int cx = 0; cx < w; cx++)
       	{
            put_pixel_RGB565(x + cx, y + cy, r, g, b);
        }
    }
}

// helper function for drawing - no more need to go mess with
// the main function when just want to change what to draw...
void draw() {

    int x, y;
    int r, g, b;
    int dr;
    int cr = vinfo.yres / 3;
    int cg = vinfo.yres / 3 + vinfo.yres / 4;
    int cb = vinfo.yres / 3 + vinfo.yres / 4 + vinfo.yres / 4;

    for (y = 0; y < (vinfo.yres); y++) {
        for (x = 0; x < vinfo.xres; x++) {
            dr = (int)sqrt((cr - x)*(cr - x)+(cr - y)*(cr - y));
            r = 255 - 256 * dr / cr;
            r = (r >= 0) ? r : 0;
            dr = (int)sqrt((cg - x)*(cg - x)+(cr - y)*(cr - y));
            g = 255 - 256 * dr / cr;
            g = (g >= 0) ? g : 0;
            dr = (int)sqrt((cb - x)*(cb - x)+(cr - y)*(cr - y));
            b = 255 - 256 * dr / cr;
            b = (b >= 0) ? b : 0;

            put_pixel_RGB565(x, y, r, g, b);
        }
    }

    fill_rect(1700, 100, 50, 50, 255, 0, 255);
    draw_line(1750, 150, 1200, 500, 3255, 0, 0);

    int green = 32;
    int red = 255;
    for (x=800, y=400; x < 1800 && y > 0; x += 30, y -= 20)
    {
        rectangle(x, y, 20, 50, red, green, 255);
	green += 10;
	red -= 10;
    }

    red = 255;
    green = 32;
    for (x=20, y=800; x < 1800; x += 30)
    {
        rectangle(x, y, 20, 50, red, green, 255);
	green -= 10;
	red += 10;
    }
}

// application entry point
int main(int argc, char* argv[])
{

    int fbfd = 0;
    struct fb_var_screeninfo orig_vinfo;
    long int screensize = 0;


    // Open the file for reading and writing
    fbfd = open("/dev/fb0", O_RDWR);
    if (fbfd == -1) {
        printf("Error: cannot open framebuffer device.\n");
        return(1);
    }
    printf("The framebuffer device was opened successfully.\n");

//#ifdef JUNK
    // hide cursor
    char *kbfds = "/dev/tty";
    int kbfd = open(kbfds, O_WRONLY);
    if (kbfd >= 0) {
        ioctl(kbfd, KDSETMODE, KD_GRAPHICS);
    }
    else {
        printf("Could not open %s.\n", kbfds);
    }
//#endif

    // Get variable screen information
    if (ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo)) {
        printf("Error reading variable information.\n");
    }
    printf("Original %dx%d, %dbpp\n", vinfo.xres, vinfo.yres, 
       vinfo.bits_per_pixel );

    // Store for reset (copy vinfo to vinfo_orig)
    memcpy(&orig_vinfo, &vinfo, sizeof(struct fb_var_screeninfo));

    // Get fixed screen information
    if (ioctl(fbfd, FBIOGET_FSCREENINFO, &finfo)) {
        printf("Error reading fixed information.\n");
    }

    // map fb to user mem 
    screensize = finfo.smem_len;
    fbp = (char*)mmap(0, 
              screensize, 
              PROT_READ | PROT_WRITE, 
              MAP_SHARED, 
              fbfd, 
              0);

    if ((int)fbp == -1) {
        printf("Failed to mmap.\n");
    }
    else {
        // draw...
        draw();
        sleep(5);
    }

    // cleanup
    // unmap fb file from memory
    munmap(fbp, screensize);

//#ifdef JUNK
    // reset cursor
    if (kbfd >= 0) {
        ioctl(kbfd, KDSETMODE, KD_TEXT);
        // close kb file
        close(kbfd);
    }
//#endif

    // reset the display mode
    if (ioctl(fbfd, FBIOPUT_VSCREENINFO, &orig_vinfo)) {
        printf("Error re-setting variable information.\n");
    }
    // close fb file    
    close(fbfd);

    return 0;
  
}
