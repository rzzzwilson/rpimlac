/*
 * display.c
 *
 * A testbed for the real rPimlac display code.
 * Runs only in 16bpp mode, 1920x1080.
 *
 * Original work by J-P Rosti (a.k.a -rst- and 'Raspberry Compote')
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <linux/kd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <stdint.h>
#include <time.h>
#include <signal.h>
#include <stdarg.h>
#include <unistd.h>
#include "vcio.h"
//#include "font_16.c"
//#include "FreeMono24.c"
#include "font_22x18.c"

// define the name of the log file
// if this line is commented out, no logging occurs
#define LOG_FILE	"display.log"
FILE *log_fp;

// 'global' variables to store screen info
char *fbp = 0;
struct fb_var_screeninfo vinfo;
struct fb_fix_screeninfo finfo;

//---------------------------------------------------------------------
// Font descriptors.
// Set by g_use_font().
//---------------------------------------------------------------------

uint8_t **FontData;
int FontWidth;
int FontHeight;

//---------------------------------------------------------------------
// The display palette.
//---------------------------------------------------------------------

// macro to pach RGB colours into the 16bit 565 format
#define RGB565(r,g,b)	((((r) & 0x1f) << 11) + (((g) & 0x3f) << 5) + ((b) & 0x1f))

// the actual palette table
uint32_t palette[] = {RGB565(31,63,31), // PHOSPHOR1
                      RGB565(23,47,23), // PHOSPHOR2
                      RGB565(17,35,17), // PHOSPHOR3
                      RGB565(13,26,13), // PHOSPHOR4
                      RGB565(10,19,10), // PHOSPHOR5
                      RGB565( 7,14, 7), // PHOSPHOR6
                      RGB565( 6,10, 6), // PHOSPHOR7
                      RGB565( 3, 5, 3), // PHOSPHOR8
		      RGB565( 0, 0, 0), // BLACK
		      RGB565(31,63,31), // WHITE
                      RGB565(31, 0, 0), // RED
                      RGB565( 0,63, 0), // GREEN
                      RGB565( 0, 0,31), // BLUE
                     };

// give real names to the indices into the "palette" table
typedef enum {PHOSPHOR1 = 0,
              PHOSPHOR2 = 1,
              PHOSPHOR3 = 2,
              PHOSPHOR4 = 3,
              PHOSPHOR5 = 4,
              PHOSPHOR6 = 5,
              PHOSPHOR7 = 6,
              PHOSPHOR8 = 7,
	      BLACK = 8,
	      WHITE = 9,
              RED = 10,
              BLUE = 11,
              GREEN = 12,
             } Colour;

// table to convert from palette index to palette name
char * colour2name[] = {"PHOSPHOR1",
                        "PHOSPHOR2",
                        "PHOSPHOR3",
                        "PHOSPHOR4",
                        "PHOSPHOR5",
                        "PHOSPHOR6",
                        "PHOSPHOR7",
                        "PHOSPHOR8",
			"BLACK",
			"WHITE",
                        "RED",
                        "BLUE",
                        "GREEN",
                       };

//---------------------------------------------------------------------
// Log text to a file
//---------------------------------------------------------------------
void logger(char *format, ...)
{
#ifdef LOG_FILE
    va_list ap;
    char buff[256];

    va_start(ap, format);
    vsprintf(buff, format, ap);
    va_end(ap);

    fputs(buff, log_fp);
//    fflush(log_fp);
//    fsync(fileno(log_fp));
#endif
}

//---------------------------------------------------------------------
// Set the font subsequent graphic operations will use.
//     font_data  a pointer to the data array to use
//     width      width of the characters
//     height     height of the characters
//---------------------------------------------------------------------

void g_use_font(uint8_t **font_data, int width, int height)
{
    FontData = font_data;
    FontWidth = width;
    FontHeight = height;
}

//---------------------------------------------------------------------
// Clear the framebuffer to a given colour.
//     c  value to clear to (RGB565)
//---------------------------------------------------------------------

void g_clear(Colour c)
{
    memset(fbp, palette[c], finfo.smem_len);
}

//---------------------------------------------------------------------
// Draw a pixel on the framebuffer.
//     x,y  screen address of the pixel to draw
//     c    Colour for the pixel
//---------------------------------------------------------------------

void g_pixel(int x, int y, Colour c)
{
    logger("g_pixel: setting pixel at (%d,%d)\n", x, y);

    // check pixel is on-screen
    if (x < 0 || y < 0 || x >= vinfo.xres || y >= vinfo.yres)
    {
        return;  // do nothing if off-screen
    }

    // calculate the pixel's byte offset inside the buffer
    unsigned int pix_offset = x*2 + y*finfo.line_length;

    // pack the RGB values into 5,6,5 width fields and store in framebuffer
    *(unsigned short*) (fbp + pix_offset) = palette[c];
}

//---------------------------------------------------------------------
// Draw a line.
// 
// Uses Bresenham's algorithm.
//---------------------------------------------------------------------

void g_line(int x0, int y0, int x1, int y1, Colour c)
{
    int dx = x1 - x0;
    dx = (dx >= 0) ? dx : -dx; // abs()

    int dy = y1 - y0;
    dy = (dy >= 0) ? dy : -dy; // abs()

    int sx;
    int sy;

    if (x0 < x1)
      sx = 1;
    else
      sx = -1;

    if (y0 < y1)
      sy = 1;
    else
      sy = -1;

    int err = dx - dy;
    int e2;
    int done = 0;

    while (!done)
    {
        g_pixel(x0, y0, c);

        if ((x0 == x1) && (y0 == y1))
        {
            done = 1;
        }
        else
        {
            e2 = 2 * err;
            if (e2 > -dy)
            {
                err = err - dy;
                x0 = x0 + sx;
            }
            if (e2 < dx)
            {
                err = err + dx;
                y0 = y0 + sy;
            }
        }
    }
}

//---------------------------------------------------------------------
// Draw a rectangle.
//---------------------------------------------------------------------

void g_rectangle(int x, int y, int w, int h, Colour c)
{
    g_line(x, y, x+w, y, c);
    g_line(x, y, x, y+h, c);
    g_line(x, y+h, x+w, y+h, c);
    g_line(x+w, y, x+w, y+h, c);
}

//---------------------------------------------------------------------
// Draw a filled rectangle.
//---------------------------------------------------------------------

void g_fill_rectangle(int x, int y, int w, int h, Colour c)
{
    for (int cy = 0; cy < h; cy++)
    {
        for (int cx = 0; cx < w; cx++)
               {
            g_pixel(x + cx, y + cy, c);
        }
    }
}

//---------------------------------------------------------------------
// Draw a circle.
//
// Use Bresenham's algorithm.
//---------------------------------------------------------------------

static
void g_draw_8_symmetric(int xc, int yc, int x, int y, Colour c)
{
    g_pixel(x+xc, y+yc, c);
    g_pixel(x+xc, -y+yc, c);
    g_pixel(-x+xc, -y+yc, c);
    g_pixel(-x+xc, y+yc, c);
    g_pixel(y+xc, x+yc, c);
    g_pixel(y+xc, -x+yc, c);
    g_pixel(-y+xc, -x+yc, c);
    g_pixel(-y+xc, x+yc, c);
}

void g_circle(int xc, int yc, int radius, Colour c)
{
    int x = 0;
    int y = radius;
    int d = 3 - 2*radius;

    g_draw_8_symmetric(xc, yc, x, y, c);

    while (x <= y)
    {
        if(d <= 0)
        {
            d += 4*x + 6;
        }
         else
        {
            d += 4*x - 4*y + 10;
            y -= 1;
        }

        x += 1;

        g_draw_8_symmetric(xc, yc, x, y, c);
    }
}

//---------------------------------------------------------------------
// Draw a character at a position.
//     x, y    position at which to draw char (top-left corner)
//     ch      the character to draw (ASCII)
//     c       the foreground colour to draw the character with
//---------------------------------------------------------------------

void g_char(int x, int y, uint8_t ch, Colour c)
{
    logger("g_char: drawing at (%d,%d), ch=%c, ch-0x20=%d\n", x, y, ch, ch-0x20);

//    uint8_t *data = font_22x18[ch - 0x20];
    uint8_t *data = FontData[ch - 0x20];
    logger("data=%p, font_22x18=%p, FontData=%p, FontHeight=%d, FontWidth=%d\n",
		    data, font_22x18, FontData, FontHeight, FontWidth);

    for (int h = 0; h < FontHeight; ++h)
    {
	int offset = 0;

	for (int i = 0; i < 3; ++i)
	{
	    uint8_t mask = 0x01;
	    uint8_t val = *data++;

	    while (mask)
	    {
	        logger("val=%02x, mask=%02x, (val&mask)=%02x\n", val, mask, val&mask);
                if (val & mask)
	        {
	            g_pixel(x+offset, y+h, c);
	        }

		offset += 1;
		mask <<= 1;
	    }
        }
    }
}

//---------------------------------------------------------------------
// Draw a text string on the screen.
//     x, y  top-left position of the text
//     txt   the string to draw
//     c     the colour to draw with
//---------------------------------------------------------------------

void g_text_string(int x, int y, char *txt, Colour c)
{
    while (*txt)
    {
	char ch = *(txt++);

        g_char(x, y, ch, c);
	x += FontWidth;
    }
}

//---------------------------------------------------------------------
// Test draw code.
//
// Draw a dynamic display.  Try to draw at 40 fps.
//---------------------------------------------------------------------

int g_x1;
int g_y1;

int g_x2;
int g_y2;

int g_x3;
int g_y3;

int g_x4;
int g_y4;

long last_draw = 0;

#define FPS                120

#define NUM_SHAPES        40


void draw(void)
{
    // get current time in microseconds
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC_RAW, &now);
    long now_usec = now.tv_sec * 1000000 + now.tv_nsec / 1000;

    // wait a bit for the next frame
//    long next_draw = last_draw + 1000000/FPS;
    long next_draw = now_usec + 1000000/FPS;

    if (now_usec <= next_draw)
    {
        // wait until the next frame is due
        usleep(next_draw - now_usec);
    }

    last_draw = now_usec;

    // draw the screen
    g_clear(BLACK);
#ifdef JUNK
    int dx1 = g_x1;
    int dy1 = g_y1;
    int dx2 = g_x2;
    int dy2 = g_y2;
    int dx3 = g_x3;
    int dy3 = g_y3;
    int dx4 = g_x4;
    int dy4 = g_y4;

    for (int del=0; del < NUM_SHAPES; ++del)
    {
        g_line(dx1-del*3, dy1+del*3, dx2-del*3, dy2-del*3, PHOSPHOR1 + (del % 4));
        g_line(dx2-del*3, dy2+del*3, dx3-del*3, dy3-del*3, PHOSPHOR1 + (del % 4));
        g_line(dx3-del*3, dy3+del*3, dx4-del*3, dy4-del*3, PHOSPHOR1 + (del % 4));
        g_line(dx4-del*3, dy4+del*3, dx1-del*3, dy1-del*3, PHOSPHOR1 + (del % 4));

        g_circle(dx1-100-del*2, dy1-100+del*2, 50, WHITE + (del % 4));
    }
#endif

//    // draw some text characters
//    for (int dx = 0; dx < 94; ++dx)
//    {
//        g_char(50 + 9*dx, 50, 'A', PHOSPHOR1);
//    }

    g_text_string(50, 100, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", PHOSPHOR1);

//    g_char(50, 100, 'H', PHOSPHOR1);
}

/* Signal Handler for SIGINT */
void sigint_handler(int sig_num)
{
    // Reset handler to catch SIGINT next time.
    // Refer http://en.cppreference.com/w/c/program/signal
    signal(SIGINT, sigint_handler);

    // nothing else, just ignore
}

int main(int argc, char* argv[])
{
#ifdef LOG_FILE
    // prepare the logger file
    log_fp = fopen(LOG_FILE, "w");
#endif

    // plug in the ^C handler
    signal(SIGINT, sigint_handler);

    // set the font to use
//    g_use_font((uint8_t **) font_22x18, 22, 18);
    g_use_font((uint8_t **) FontData, 22, 18);

    // framebuffer stuff
    int fbfd = 0;
    struct fb_var_screeninfo orig_vinfo;
    long int screensize = 0;

    // Open the file for reading and writing
    fbfd = open("/dev/fb0", O_RDWR);
    if (fbfd == -1) {
        printf("Error: cannot open framebuffer device.\n");
        return(1);
    }

    // hide cursor
    char *kbfds = "/dev/tty";
    int kbfd = open(kbfds, O_WRONLY);
    if (kbfd >= 0)
    {
        ioctl(kbfd, KDSETMODE, KD_GRAPHICS);
    }
    else
    {
        printf("Could not open %s.\n", kbfds);
    }

    // Get variable screen information
    if (ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo))
    {
        printf("Error reading variable information.\n");
    }
    printf("Original %dx%d, %dbpp\n", vinfo.xres, vinfo.yres, vinfo.bits_per_pixel );

    // Store for reset (copy vinfo to vinfo_orig)
    memcpy(&orig_vinfo, &vinfo, sizeof(struct fb_var_screeninfo));

    // Get fixed screen information
    if (ioctl(fbfd, FBIOGET_FSCREENINFO, &finfo))
    {
        printf("Error reading fixed information.\n");
    }

    // map fb to user mem 
    screensize = finfo.smem_len;
    fbp = (char*) mmap(0, screensize, PROT_READ | PROT_WRITE, MAP_SHARED, fbfd, 0);

    if (fbp == (char *) -1)
    {
        printf("Failed to mmap.\n");
    }
    else
    {
        g_x1 = vinfo.xres/2;
        g_y1 = vinfo.yres/2 - 250;

        g_x2 = vinfo.xres/2 + 250;
        g_y2 = vinfo.yres/2;

        g_x3 = vinfo.xres/2;
        g_y3 = vinfo.yres/2 + 250;

        g_x4 = vinfo.xres/2 - 250;
        g_y4 = vinfo.yres/2;

//        for (int q=0; q < 1000; ++q)
        for (int q=0; q < 1; ++q)
        {
            draw();
            g_x1 -= 1;
            g_y2 -= 1;
            g_x3 += 1;
            g_y4 += 1;
        }
        sleep(5);
        g_clear(BLACK);
    }

    // cleanup
    // unmap fb file from memory
    munmap(fbp, screensize);

    // reset cursor
    if (kbfd >= 0) {
        ioctl(kbfd, KDSETMODE, KD_TEXT);
        // close kb file
        close(kbfd);
    }

    // reset the display mode
    if (ioctl(fbfd, FBIOPUT_VSCREENINFO, &orig_vinfo)) {
        printf("Error re-setting variable information.\n");
    }
    // close fb file    
    close(fbfd);

#ifdef LOG_FILE
    // close the log file
    fclose(log_fp);
#endif

    return 0;
}
