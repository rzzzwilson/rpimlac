#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "FreeMono24.c"


void print_bits(uint32_t buff)
{
    uint32_t mask = 1 << 31;

    printf("%08x=", buff);

    for (int i = 0; i < 32; ++i)
    {
	if (buff & mask)
	{
	    printf("1");
	}
	else
	{
	    printf("0");
	}
	mask >>= 1;
    }
    printf("\n");
}

int main(void)
{
    char *str = "!HX";
    //uint8_t *bptr = freemono24_bmp[ch - 0x20];

for (int x = 0; x < strlen(str); ++x)
{
    char ch = str[x];
    uint8_t *bptr = freemono24_bmp[ch - 0x20];
    int boff = 0;
    uint32_t buffer;
    uint8_t tmp;        // staging area for byte/part of byte

    printf("%c ----------------------------------------------\n", ch);

    for (int l = 0; l < FONT_HEIGHT; ++l)
    {
        int rem = 18; //FONT_WIDTH;
	uint8_t tmp = 0;

//	printf("-------------------------------------------------------\n");
//	printf("point0: buffer=%08x, bptr=%p, *bptr=%02x, boff=%d, rem=%d, tmp=%02x\n", buffer, bptr, *bptr, boff, rem, tmp);

        // get the next partial byte into the buffer
        if (boff != 0)
        {
            tmp = *bptr++;
	    tmp >>= (8 - boff);
	    buffer = tmp;
            rem -= boff;
            boff = 0;
        }

	// now get as many full bytes as we can
	while (rem >= 8)
	{
//	printf("point1: buffer=%08x, bptr=%p, *bptr=%02x, boff=%d, rem=%d, tmp=%02x\n", buffer, bptr, *bptr, boff, rem, tmp);

            buffer <<= 8;	// make room for new byte
	    buffer += *bptr++;
	    rem -= 8;
	}

//	printf("point2: buffer=%08x, bptr=%p, *bptr=%02x, boff=%d, rem=%d, tmp=%02x\n", buffer, bptr, *bptr, boff, rem, tmp);

	// finally, get remaining part byte, 'rem' in [0,7]
	if (rem > 0)
	{
	    tmp = *bptr;
	    tmp >>= (8 - rem);
	    boff = rem;
	    buffer <<= rem;
	    buffer += tmp;
	}

//	printf("point3: buffer=%08x, bptr=%p, *bptr=%02x, boff=%d, rem=%d, tmp=%02x\n", buffer, bptr, *bptr, boff, rem, tmp);

	print_bits(buffer);
    }
}
    return 0;
}
