To use framebuffer you need to be running in a text-only mode.
To configure booting to this mode, do:

    sudo systemctl set-default multi-user.target
    sudo reboot

To undo the above, getting back to normal graphical mode, do:

    sudo systemctl set-default graphical.target
    sudo reboot

This is for Linux systems using *systemd*.

---------------------------

There is a utility in ~/bin/bootmode to make this easier.

---------------------------

Alternatively, at the graphical login screen press CTRL-FN-ALT-F1, ..., F6 to
get to a text login. CTRL-FN-ALT-F7 gets back to the graphical login.
When in text mode type in *startx* to get the desktop.
